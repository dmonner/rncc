package dmonner.rncc.util;

import java.util.List;
import java.util.Random;

public class RandomTools
{
	private static Random r = new Random();

	public static double fromUniform(final double min, final double max)
	{
		return min + nextDouble() * (max - min);
	}

	public static Random generator()
	{
		return r;
	}

	public static void init(final long seed)
	{
		r = new Random(seed);
	}

	public static boolean nextBoolean()
	{
		return r.nextBoolean();
	}

	public static double nextDouble()
	{
		return r.nextDouble();
	}

	public static <E> E nextElement(final E[] array)
	{
		return array[nextIntBelow(array.length)];
	}

	public static <E> E nextElement(final List<E> list)
	{
		return list.get(nextIntBelow(list.size()));
	}

	public static float nextFloat()
	{
		return r.nextFloat();
	}

	public static float[] nextFloat(final int length)
	{
		final float[] v = new float[length];
		for(int i = 0; i < length; i++)
		{
			v[i] = r.nextFloat();
		}
		return v;
	}

	public static <E> int nextIndex(final E[] array)
	{
		return nextIntBelow(array.length);
	}

	public static int nextInt()
	{
		return r.nextInt();
	}

	public static int nextIntBelow(final int n)
	{
		return r.nextInt(n);
	}

	public static int nextIntBetween(final int low, final int high)
	{
		return r.nextInt(high - low) + low;
	}

	public static int[] permutation(final int size)
	{
		final int[] perm = new int[size];
		for(int i = 0; i < size; i++)
			perm[i] = i;
		randomize(perm, size);
		return perm;
	}

	public static <E> void randomize(final E[] array)
	{
		randomize(array, array.length);
	}

	public static <E> void randomize(final E[] array, final int size)
	{
		randomize(array, 0, size);
	}

	public static <E> void randomize(final E[] array, final int start, final int size)
	{
		for(int i = start; i < start + size - 1; i++)
		{
			final int j = nextIntBetween(i, start + size);
			final E tmp = array[i];
			array[i] = array[j];
			array[j] = tmp;
		}
	}

	public static void randomize(final int[] array)
	{
		randomize(array, array.length);
	}

	public static void randomize(final int[] array, final int size)
	{
		for(int i = 0; i < size - 1; i++)
		{
			final int j = nextIntBetween(i, size);
			final int tmp = array[i];
			array[i] = array[j];
			array[j] = tmp;
		}
	}

	public static <E> void randomize(final List<E> list)
	{
		randomize(list, list.size());
	}

	public static <E> void randomize(final List<E> list, final int size)
	{
		randomize(list, 0, size);
	}

	public static <E> void randomize(final List<E> list, final int start, final int size)
	{
		for(int i = start; i < start + size - 1; i++)
		{
			final int j = nextIntBetween(i, start + size);
			final E tmp = list.get(i);
			list.set(i, list.get(j));
			list.set(j, tmp);
		}
	}

	/**
	 * Given an array of arrays, randomly permutes each of the subarrays in parallel (i.e. each
	 * subarray is placed into the same random permutation as the others). This preserves the ability
	 * to parallel index between elements of the subarrays.
	 * 
	 * @param <E>
	 * @param array
	 */
	public static <E> void randomizeParallel(final E[][] array)
	{
		randomizeParallel(array, array[0].length);
	}

	/**
	 * Given an array of arrays, randomly permutes the first size elements of each of the subarrays in
	 * parallel (i.e. each subarray is placed into the same random permutation as the others). This
	 * preserves the ability to parallel index between elements of the subarrays.
	 * 
	 * @param <E>
	 * @param array
	 * @param size
	 */
	public static <E> void randomizeParallel(final E[][] array, final int size)
	{
		for(int i = 0; i < size - 1; i++)
		{
			final int j = nextIntBetween(i, size);
			for(int e = 0; e < array.length; e++)
			{
				final E[] ae = array[e];
				final E tmp = ae[i];
				ae[i] = ae[j];
				ae[j] = tmp;
			}
		}
	}

	public static void setSeed(final long seed)
	{
		r.setSeed(seed);
	}

	public static boolean withProbability(final float probability)
	{
		return r.nextFloat() < probability;
	}
}
