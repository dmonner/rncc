package dmonner.rncc.util;

public class BinaryTools
{
	public static float mid = 0.5F;

	public static float[] fromBitString(final String s)
	{
		final float[] f = new float[s.length()];
		for(int i = 0; i < s.length(); i++)
			if(s.charAt(i) == '1')
				f[i] = 1F;
		return f;
	}

	public static String toBitString(final float[] bits)
	{
		return toBitString(bits, mid, mid);
	}

	public static String toBitString(final float[] bits, final double lowval, final double highval)
	{
		return toBitString(bits, 0, bits.length, lowval, highval);
	}

	public static String toBitString(final float[] bits, final int start, final int end)
	{
		return toBitString(bits, start, end, mid, mid);
	}

	public static String toBitString(final float[] bits, final int start, final int end,
			final double lowval, final double highval)
	{
		if(lowval > highval)
			throw new IllegalArgumentException("Parameter lowval must be <= highval.");

		final StringBuffer s = new StringBuffer();

		for(int i = start; i < end; i++)
			if(bits[i] >= highval)
				s.append("1");
			else if(bits[i] <= lowval)
				s.append("0");
			else
				s.append("?");

		return s.toString();
	}
}
