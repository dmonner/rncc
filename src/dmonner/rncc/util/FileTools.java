package dmonner.rncc.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class FileTools
{
	public static String createDirectory(final String name)
	{
		if(name != null)
		{
			final File f = new File(name);
			f.mkdirs();
			return f.getAbsolutePath();
		}
		return "";
	}

	public static PrintWriter createFile(final String filename)
	{
		return createFile(filename, false);
	}

	public static PrintWriter createFile(final String filename, final boolean append)
	{
		try
		{
			createDirectory(new File(filename).getParent());
			return new PrintWriter(
					new OutputStreamWriter(new FileOutputStream(filename, append), "UTF8"), true);
		}
		catch(final FileNotFoundException fnfe)
		{
			fnfe.printStackTrace();
			System.exit(1);
		}
		catch(final IOException ioe)
		{
			ioe.printStackTrace();
			System.exit(1);
		}
		return null;
	}
}
