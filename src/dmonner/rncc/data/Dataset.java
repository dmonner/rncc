package dmonner.rncc.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import dmonner.rncc.util.BinaryTools;

/**
 * An instance of this class holds a dataset, consisting of a collection of entities, their features
 * and classes, and the directed, weighted relationship graph(s) connecting them. A dataset is most
 * easily defined by a single .dat file along with one or more .net files.
 * 
 * @author dmonner
 */
public class Dataset
{
	private final String name;
	private int nEntities;
	private final int nRelations;
	private int nFeatures;
	private int nClasses;
	private Map<String, Integer> ids;
	private float[][] features;
	private float[][] classes;
	private final float[][][] relations;

	/**
	 * Creates a dataset using a single .dat file and .net file.
	 * 
	 * @param datfile
	 * @param netfile
	 */
	public Dataset(final String datfile, final String netfile)
	{
		name = datfile;
		readDatFile(datfile);
		nRelations = 1;
		relations = new float[nRelations][nEntities][nEntities];
		readNetFile(netfile, 0);
	}

	/**
	 * Creates a dataset with potentially more than one relationship graph; requires a .dat file and
	 * one or more .net files.
	 * 
	 * @param datfile
	 * @param netfiles
	 */
	public Dataset(final String datfile, final String[] netfiles)
	{
		name = datfile;
		readDatFile(datfile);
		nRelations = netfiles.length;
		relations = new float[nRelations][nEntities][nEntities];
		for(int i = 0; i < netfiles.length; i++)
			readNetFile(netfiles[i], i);
	}

	/**
	 * Returns a neural representation of the target classes assigned to entity i.
	 * 
	 * @param i
	 * @return
	 */
	public float[] getClasses(final int i)
	{
		return classes[i];
	}

	/**
	 * Returns the edge weight from entity i to entity j in the r-th relationship graph. Returns 0F if
	 * no such edge exists.
	 * 
	 * @param r
	 * @param i
	 * @param j
	 * @return
	 */
	public float getEdgeWeight(final int r, final int i, final int j)
	{
		return relations[r][i][j];
	}

	/**
	 * Returns a neural representation of the input features assigned to entity i.
	 * 
	 * @param i
	 * @return
	 */
	public float[] getFeatures(final int i)
	{
		return features[i];
	}

	public String getName()
	{
		return name;
	}

	/**
	 * Returns true iff an edge exists from entity i to entity j in the r-th relationship graph.
	 * 
	 * @param r
	 * @param i
	 * @param j
	 * @return
	 */
	public boolean isConnected(final int r, final int i, final int j)
	{
		return relations[r][i][j] != 0F;
	}

	/**
	 * Returns the number of possible classes in this dataset.
	 * 
	 * @return
	 */
	public int nClasses()
	{
		return nClasses;
	}

	/**
	 * Returns the number of entities in this dataset.
	 * 
	 * @return
	 */
	public int nEntities()
	{
		return nEntities;
	}

	/**
	 * Returns the number of input features in this dataset.
	 * 
	 * @return
	 */
	public int nFeatures()
	{
		return nFeatures;
	}

	/**
	 * Returns the number of relationship graphs (different types of relations between entities) in
	 * this dataset.
	 * 
	 * @return
	 */
	public int nRelations()
	{
		return nRelations;
	}

	/**
	 * Reads a .dat file. A .dat file is a CSV file beginning with a header on the first line, with
	 * each subsequent line containing 3 fields, each specifying a single entity: instance number (a
	 * unique integer; these should range from 0 up to number-of-entities-minus-one), features (a
	 * binary string representing active input features), and classes (another binary string
	 * representing active classes).
	 * 
	 * @param filename
	 */
	private void readDatFile(final String filename)
	{
		BufferedReader br = null;
		ids = new HashMap<String, Integer>();
		nEntities = 0;
		int canskip = 1;
		final ArrayList<float[]> features = new ArrayList<float[]>();
		final ArrayList<float[]> classes = new ArrayList<float[]>();
		try
		{
			br = new BufferedReader(new FileReader(filename));
			String line = null;
			int lno = 1;
			while((line = br.readLine()) != null)
			{
				lno++;
				line = line.trim();
				if(!line.isEmpty() && !line.startsWith("#"))
				{
					final String[] fields = line.split(",");

					// test whether first field is a number; if not, possibly skip this line as a header
					try
					{
						Integer.parseInt(fields[0]);
					}
					catch(final NumberFormatException ex)
					{
						if(canskip > 0)
						{
							canskip--;
							continue;
						}
						else
						{
							throw ex;
						}
					}

					if(fields.length == 3)
					{
						ids.put(fields[0], nEntities++);
						features.add(BinaryTools.fromBitString(fields[1]));
						classes.add(BinaryTools.fromBitString(fields[2]));
					}
					else if(fields.length > 3)
					{
						nFeatures = fields.length - 2;
						ids.put(fields[0], nEntities++);
						final float[] f = new float[nFeatures];
						for(int i = 0; i < f.length; i++)
							f[i] = Float.parseFloat(fields[i + 1]);
						features.add(f);
						classes.add(BinaryTools.fromBitString(fields[fields.length - 1]));
					}
					else
					{
						throw new IllegalArgumentException("Unsupported number of fields (" + fields.length
								+ ") found on line " + lno);
					}
				}
			}
			this.features = features.toArray(new float[features.size()][]);
			this.classes = classes.toArray(new float[classes.size()][]);
			nFeatures = this.features[0].length;
			nClasses = this.classes[0].length;
		}
		catch(final IOException ex)
		{
			ex.printStackTrace(System.out);
		}
	}

	/**
	 * Reads a .net file. A .net is a CSV file beginning with a header line, where each subsequent
	 * line consists of 3 fields, each specifying a directed link in the relationship graph: the
	 * instance number of the "from" entity (row in an adjacency matrix), the instance number of the
	 * "to" entity (column in an adjacency matrix), and the edge weight (or 1.0 for unweighted
	 * graphs).
	 * 
	 * @param filename
	 * @param r
	 *          Indicates that this graph should be stored in the r-th index internally.
	 */
	private void readNetFile(final String filename, final int r)
	{
		BufferedReader br = null;
		int canskip = 1;
		try
		{
			br = new BufferedReader(new FileReader(filename));
			String line = null;
			while((line = br.readLine()) != null)
			{
				line = line.trim();
				if(!line.isEmpty() && !line.startsWith("#"))
				{
					final String[] fields = line.split(",");

					// test whether first field is a number; if not, possibly skip this line as a header
					try
					{
						Integer.parseInt(fields[0]);
					}
					catch(final NumberFormatException ex)
					{
						if(canskip > 0)
						{
							canskip--;
							continue;
						}
						else
						{
							throw ex;
						}
					}

					relations[r][ids.get(fields[0])][ids.get(fields[1])] = Float.parseFloat(fields[2]);
				}
			}
		}
		catch(final IOException ex)
		{
			ex.printStackTrace(System.out);
		}
	}
}
