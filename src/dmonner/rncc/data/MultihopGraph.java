package dmonner.rncc.data;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import dmonner.rncc.util.BinaryTools;
import dmonner.rncc.util.FileTools;
import dmonner.rncc.util.RandomTools;

/**
 * This class generates synthetic datasets where a given number of entities share a set of input
 * features and are connected to each other in a random, directed, unweighted manner. Each entity's
 * class is defined as the most prominent feature among its neighbors within a given neighborhood
 * radius.
 * 
 * @author dmonner
 */
public class MultihopGraph
{
	public static void main(final String[] args)
	{
		// Settings used to generate Onehop dataset
		final int features = 4; // number of features per entity
		final int entities = 1000; // number of entities to generate
		final int radius = 1; // the radius of the neighborhood on which to base classes
		final float density = 0.01F; // connection probability between entities
		final boolean intermediate = true; // determines whether an entity's class is driven by
		// neighbors at precisely the radius distance away (false), or any intermediate point within
		// the radius distance (true)

		// Settimgs ised tp generate Twohop dataset
		// final int features = 4; // number of features per entity
		// final int entities = 1000; // number of entities to generate
		// final int radius = 2; // the radius of the neighborhood on which to base classes
		// final float density = 0.003F; // connection probability between entities
		// final boolean intermediate = true; // determines whether an entity's class is driven by
		// neighbors at precisely the radius distance away (false), or any intermediate point within
		// the radius distance (true)

		// Generate and print the graph
		final MultihopGraph g = new MultihopGraph(features, entities, radius, density, intermediate);
		System.out.println(g);

		// Ask if we want to save it to a file
		System.out.print("\n=============================\n\nWrite file? [y|N] ");
		final Scanner in = new Scanner(System.in);
		final String response = in.next();
		if(response.startsWith("y") || response.startsWith("Y"))
		{
			System.out.print("File basename? ");
			final String base = in.next();
			g.toFiles("data/" + base);
		}
	}

	private final int nodes;
	private final float[][] freps;
	private final float[][] creps;
	private final int[][] totals;
	private final int[] features;
	private final int[] featuredist;
	private final int[] classes;
	private final int[] classdist;
	private final boolean[][] adjacency;

	public MultihopGraph(final int nfeatures, final int nodes, final int hood, final float density,
			final boolean useIntermediate)
	{
		// Initialize
		this.nodes = nodes;
		freps = new float[nodes][nfeatures];
		creps = new float[nodes][nfeatures];
		totals = new int[nodes][nfeatures];
		features = new int[nodes];
		featuredist = new int[nfeatures];
		classes = new int[nodes];
		classdist = new int[nfeatures];
		adjacency = new boolean[nodes][nodes];

		// Generate random feature assignment for each node
		for(int i = 0; i < nodes; i++)
		{
			features[i] = RandomTools.nextIntBelow(nfeatures);
			featuredist[features[i]]++;
			freps[i][features[i]] = 1F;
		}

		// Generate random links
		for(int i = 0; i < nodes; i++)
			for(int j = 0; j < nodes; j++)
				if(i != j && RandomTools.withProbability(density))
					adjacency[i][j] = true;

		// Assign the classes, using BFS to search to a fixed depth from each node
		for(int n = 0; n < nodes; n++)
		{
			final int[] parent = new int[nodes];
			final int[] depth = new int[nodes];
			final boolean[] visited = new boolean[nodes];

			final List<Integer> queue = new ArrayList<Integer>(nodes);
			queue.add(n);
			visited[n] = true;

			while(!queue.isEmpty())
			{
				final int i = queue.remove(0);
				final int d = depth[i];

				// if we're at the limit depth (or using intermediate nodes), stop and look at my feature
				if(useIntermediate || depth[i] == hood)
					totals[n][features[i]]++;

				// keep searching if below depth limit
				if(depth[i] < hood)
				{
					for(int j = 0; j < nodes; j++)
					{
						if(adjacency[i][j] && !visited[j])
						{
							parent[j] = i;
							depth[j] = d + 1;
							queue.add(j);
							visited[j] = true;
						}
					}
				}
			}

			// Now that we have the totals for the right neighborhood depth, assign class
			int biggest = -1;
			for(int i = 0; i < nfeatures; i++)
			{
				// NOTE: ties go to the lower-numbered class here
				if(totals[n][i] > biggest)
				{
					biggest = totals[n][i];
					classes[n] = i;
				}
			}

			classdist[classes[n]]++;
			creps[n][classes[n]] = 1F;
		}

	}

	/**
	 * Converts the dataset into the files basename.dat and basename.net for use with RNCC.
	 * 
	 * @param basename
	 */
	public void toFiles(final String basename)
	{
		final String dat = basename + ".dat";
		final String net = basename + ".net";

		// print dat file
		final PrintWriter datfile = FileTools.createFile(dat);
		datfile.println("instance,features,classes");

		for(int n = 0; n < nodes; n++)
			datfile.println(n + "," + //
					BinaryTools.toBitString(freps[n]) + "," + //
					BinaryTools.toBitString(creps[n]));

		datfile.close();

		// print net file
		final PrintWriter netfile = FileTools.createFile(net);
		netfile.println("row,col,value");

		for(int i = 0; i < nodes; i++)
			for(int j = 0; j < nodes; j++)
				if(adjacency[i][j])
					netfile.println(i + "," + j + ",1.0");

		netfile.close();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder();

		// sb.append("Features: ");
		// sb.append(Arrays.toString(features));
		// sb.append("\n");
		//
		// sb.append("Adjacency: \n");
		// sb.append(MatrixTools.toString(adjacency));
		// sb.append("\n");

		int totalhood = 0;

		for(int n = 0; n < nodes; n++)
		{
			sb.append(n);
			sb.append(" => ");
			sb.append(Arrays.toString(totals[n]));

			for(int i = 0; i < totals[n].length; i++)
				totalhood += totals[n][i];

			sb.append(" => ");
			sb.append(classes[n]);
			sb.append("\n");
		}

		sb.append("\nFeature distribution: ");
		sb.append(Arrays.toString(featuredist));
		sb.append("\n");

		sb.append("\nClass distribution: ");
		sb.append(Arrays.toString(classdist));
		sb.append("\n");

		sb.append("\nAverage neighborhood size: ");
		sb.append(((float) totalhood) / nodes);
		sb.append("\n");

		return sb.toString();
	}
}
