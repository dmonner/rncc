package dmonner.rncc;

import java.util.Arrays;
import java.util.List;

import dmonner.rncc.util.RandomTools;
import dmonner.xlbp.Network;
import dmonner.xlbp.layer.Layer;
import dmonner.xlbp.trial.Step;
import dmonner.xlbp.trial.Trial;

public class NeighborTrial extends Trial
{
	private final Layer classRec;
	private final Layer repRec;

	private float[] repDep;
	private float[] classDep;

	private int neighborStepStart;
	private int neighborStepNum;

	public NeighborTrial(final Network meta, final Layer classRec, final Layer repRec)
	{
		super(meta);
		this.classRec = classRec;
		this.repRec = repRec;
	}

	public void clearLearnedRelations()
	{
		if(classDep != null)
			Arrays.fill(classDep, 0F);

		if(repDep != null)
			Arrays.fill(repDep, 0F);
	}

	public Step getLastStep()
	{
		final List<Step> steps = getSteps();
		return steps.get(steps.size() - 1);
	}

	public void randomizeNeighborSteps()
	{
		RandomTools.randomize(getSteps(), neighborStepStart, neighborStepNum);
	}

	public void setClassDepRef(final float[] classDep)
	{
		this.classDep = classDep;
	}

	public void setNeighborSteps(final int start, final int num)
	{
		neighborStepStart = start;
		neighborStepNum = num;
	}

	public void setRepDepRef(final float[] repDep)
	{
		this.repDep = repDep;
	}

	public void setTrueClassDep()
	{
		if(classDep != null)
		{
			final float[] clss = getLastStep().getTarget().getValue();
			System.arraycopy(clss, 0, classDep, 0, clss.length);
		}
	}

	public void updateClassDep()
	{
		if(classDep != null)
		{
			final float[] clss = getLastStep().getLastRecording().getRecording(classRec);
			System.arraycopy(clss, 0, classDep, 0, clss.length);
		}
	}

	public void updateDependency()
	{
		updateClassDep();
		updateRepDep();
	}

	public void updateRepDep()
	{
		if(repDep != null)
		{
			final float[] rep = getLastStep().getLastRecording().getRecording(repRec);
			System.arraycopy(rep, 0, repDep, 0, rep.length);
		}
	}
}
