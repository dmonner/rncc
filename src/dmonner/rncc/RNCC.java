package dmonner.rncc;

import java.io.IOException;
import java.io.PrintWriter;

import dmonner.rncc.data.Dataset;
import dmonner.rncc.util.FileTools;
import dmonner.rncc.util.Properties;
import dmonner.xlbp.Component;
import dmonner.xlbp.Network;
import dmonner.xlbp.NetworkDotBuilder;
import dmonner.xlbp.UniformWeightInitializer;
import dmonner.xlbp.WeightInitializer;
import dmonner.xlbp.WeightUpdaterType;
import dmonner.xlbp.compound.InputCompound;
import dmonner.xlbp.compound.MemoryCellCompound;
import dmonner.xlbp.compound.TargetCompound;
import dmonner.xlbp.compound.XEntropyTargetCompound;
import dmonner.xlbp.layer.InputLayer;
import dmonner.xlbp.layer.Layer;
import dmonner.xlbp.stat.BitDistStat;
import dmonner.xlbp.stat.BitStat;
import dmonner.xlbp.stat.TestStat;
import dmonner.xlbp.stat.TrialStat;
import dmonner.xlbp.trial.Step;
import dmonner.xlbp.trial.Trainer;
import dmonner.xlbp.trial.Trial;
import dmonner.xlbp.trial.TrialSet;
import dmonner.xlbp.util.TableWriter;

public class RNCC
{
	public static Properties getDefaultConfig()
	{
		final Properties cfg = new Properties();

		// Set parameter defaults
		cfg.set("nEpochs", "25",
				"The maximum number of passes through the data which the network is allowed.");
		cfg.set("datFile", "data/redblue1.dat",
				"The file specifying the entities, features, and classes that define the input dataset.");
		cfg.set("netFile", "data/redblue1.net",
				"The file specifying the relationship graph over the entities in the input dataset. " + //
						"This key can have multiple filenames, separated by a colon (:).");
		cfg.set("mySize", "0",
				"The size of the hidden layer that processes entity input features. Value of 0 means " + //
						"there is not such layer.");
		cfg.set("theirSize", "20", "The size of the hidden layer that aggregates neighbor inputs.");
		cfg.set("repSize", "20",
				"The size of the hidden layer that integrates entity and neighbor inputs.");
		cfg.set("myType", "IO",
				"The memory cell \"type\" string for the hidden layer that processes entity input " + //
						"features; see dmonner.xlbp.compound.MemoryCellCompound for more information.");
		cfg.set("theirType", "IO",
				"The memory cell \"type\" string for the hidden layer that aggregates neighbor " + //
						"inputs; see dmonner.xlbp.compound.MemoryCellCompound for more information.");
		cfg.set("repType", "IO",
				"The memory cell \"type\" string for the hidden layer that integrates entity and " + //
						"neighbor inputs; see dmonner.xlbp.compound.MemoryCellCompound for more information.");
		cfg.set("useNeighborFeatures", "true",
				"Whether to use an entity's neighbors' features as a supplementary input.");
		cfg.set("useNeighborClasses", "true",
				"Whether to use an entity's neighbors' predicted classes as a supplementary input.");
		cfg.set("useNeighborRepresentations", "true",
				"Whether to use an entity's neighbors' learned representations as a supplementary input.");
		cfg.set("learningRate", "0.1F", "Network's learning rate.");
		cfg.set("momentum", "0.0F",
				"Network's learning momentum; to use this, you must also set the option " + //
						"updaterType=momentum.");
		cfg.set("connectionProbability", "0.8F",
				"The density of the network's trainable weighted connection matrices.");
		cfg.set("updaterType", "basic",
				"Network's learning strategy; see dmonner.xlbp.WeightUpdaterType for more information.");
		cfg.set("xvalSplit", "8/1/1",
				"Number of folds used for training/validation/testing. Default is 8/1/1, for 10-fold " + //
						"cross-validation using 8 folds to train, 1 fold to validate, and one to test.");
		cfg.set("foldSplit", "",
				"Used to set specific split boundaries for cross-validation folds. Example: " + //
						"150/140/130 splits the data into three folds, consisting of entities 0-149, " + //
						"150-289, and 290-419. Parameter xvalSplit must sum to the number of fields " + //
						"expressed here.");
		cfg.set("wta", "true",
				"Whether to use winner-take-all evaluation. Recommended only when entities have " + //
						"exactly one class.");
		cfg.set("useTrueClassesTrain", "true",
				"If true, during training, class input values for neighbor entities that are in " + //
						"the training/known set use the ground-truth class; neighbor entities not in " + //
						"the training/known set use last-predicted class. If false, all neighbor " + //
						"input classes are last-predicted during training.");
		cfg.set("useTrueClassesTest", "true",
				"If true, during testing, class input values for neighbor entities that are in " + //
						"the training/known set use the ground-truth class; neighbor entities not " + //
						"in the training/known set use last-predicted class. If false, all neighbor " + //
						"input classes are last-predicted during testing.");
		cfg.set("recordFinalClassifications", "false",
				"Whether or not to record and output a complete record of the final " + //
						"classifications assigned to each entity.");
		cfg.set("trainingLogs", "false",
				"Whether or not to produce training and validation summaries for each fold and epoch.");
		cfg.set("networkLog", "false",
				"Whether or not to produce a log detailing the structure and size of the " + //
						"underlying XLBP neural network.");
		cfg.set("networkDot", "false",
				"Whether or not to produce a file to be used to visualize the underlying XLBP network " + //
						"using the GraphViz utility \"dot\".");
		cfg.set("randomizeNeighbors", "true",
				"If true, neighbor inputs are randomized with each presentation. If false, " + //
						"the order of neighbor presentation remains fixed throughout training and testing.");
		cfg.set("trainToFeatures", "false",
				"If true, a second output layer is added which causes the network to " + //
						"auto-encode each entity's input features and reproduce them on output. " + //
						"Potentially useful for increasing the information stored in learned " + //
						"neighbor representations.");
		cfg.set("integrateNeighborInputs", "true",
				"If true, neighbor inputs feed into a single aggregation layer; if false, " + //
						"each input feeds its own aggregation layer.");
		cfg.set("simulDependencyUpdates", "false",
				"If true, all last-prediction information used as input is only updated at " + //
						"the end of an epoch. If false, this information is updated as soon as " + //
						"new information is available.");

		return cfg;
	}

	public static void main(final String[] args) throws IOException
	{
		String configfile = "rncc.properties";

		if(args.length > 0)
		{
			if(args[0].equalsIgnoreCase("printconfig"))
			{
				getDefaultConfig().save(System.out, "RNCC Default Configuration");
				return;
			}

			configfile = args[0];
		}

		final RNCC rncc = new RNCC(configfile);
		rncc.run();
	}

	private Properties cfg;

	private Network featurenet;
	private Network classnet;
	private Network repnet;
	private Network ionet;
	private Network theirnet;
	private Network mynet;
	private Network outnet;
	private Network metanet;
	private Dataset dataset;
	private Layer rephid;
	private NeighborTrial[] trials;
	private TargetCompound output;
	private TargetCompound foutput;

	private int epochs;
	private int repSize;
	private boolean useMyHidden;
	private boolean useNeighbors;
	private boolean useNeighborClasses;
	private boolean useNeighborFeatures;
	private boolean useNeighborRepresentations;
	private boolean simulDependencyUpdates;
	private boolean useTrueClassesTrain;
	private boolean useTrueClassesTest;
	private boolean randomizeNeighbors;
	private boolean trainToFeatures;
	private boolean recordFinalClassifications;

	public RNCC(final String configfile) throws IOException
	{
		setupConfig(configfile);
		setupDataset();
		setupTask();
		setupNetwork();
		setupTrial();
	}

	private void addToStep(final boolean cond, final Step step, final InputLayer layer,
			final float[] f)
	{
		if(cond)
			step.addInput(layer, f);
	}

	private void addToTheirInputNetwork(final Network net, final String name, final int relation,
			final int size)
	{
		final InputCompound layer = new InputCompound(name + relation, size);
		net.add(layer);
		theirnet.add(layer);
	}

	private void connectTheirInputNetwork(final Network net, final MemoryCellCompound layer)
	{
		for(final Component comp : net.getComponents())
			layer.addUpstreamWeights((InputCompound) comp);
	}

	private MemoryCellCompound makeTheirHidden(final String name, final int size, final String type,
			final MemoryCellCompound downstream)
	{
		final MemoryCellCompound layer = new MemoryCellCompound("Their" + name + "Hidden", size, type);
		theirnet.add(layer);
		downstream.addUpstreamWeights(layer);
		return layer;
	}

	private NeighborTrial makeTrial(final int inst, final float[][] classDeps, final float[][] repDeps)
	{
		final NeighborTrial trial = new NeighborTrial(metanet, output.getActLayer(), rephid);
		trial.setClear(true);

		if(useNeighborClasses)
			trial.setClassDepRef(classDeps[inst]);

		if(useNeighborRepresentations)
			trial.setRepDepRef(repDeps[inst]);

		final float[] my = dataset.getFeatures(inst);

		/*
		 * input layer order, if exists:
		 * 
		 * outermost loop: relation number distinction
		 * 
		 * middle loop: feature/class/rep distinction
		 * 
		 * innermost loop: from/to neighbor distinction
		 */

		// add features of the instance's incoming neighbors
		int neighborSteps = 0;
		if(useNeighbors)
		{
			for(int j = 0; j < dataset.nEntities(); j++)
			{
				for(int r = 0; r < dataset.nRelations(); r++)
				{
					final boolean connectedFrom = dataset.isConnected(r, inst, j);
					final boolean connectedTo = dataset.isConnected(r, j, inst);

					if(connectedFrom || connectedTo)
					{
						final Step step = trial.nextStep();
						neighborSteps++;
						step.setNetwork(theirnet);

						if(useNeighborFeatures)
							addToStep(true, step, featurenet.getInputLayer(r), dataset.getFeatures(j));

						if(useNeighborClasses)
							addToStep(true, step, classnet.getInputLayer(r), classDeps[j]);

						if(useNeighborRepresentations)
							addToStep(true, step, repnet.getInputLayer(r), repDeps[j]);

						final float[] iorep = new float[2];
						iorep[0] = dataset.getEdgeWeight(r, inst, j);
						iorep[1] = dataset.getEdgeWeight(r, j, inst);
						addToStep(true, step, ionet.getInputLayer(r), iorep);
					}
				}
			}
		}

		trial.setNeighborSteps(0, neighborSteps);

		// add the desired classifications
		final Step outstep = trial.nextStep();
		outstep.addInput(mynet.getInputLayer(), my);
		outstep.setNetwork(outnet);
		outstep.addTarget(dataset.getClasses(inst));
		if(trainToFeatures)
			outstep.addTarget(1, dataset.getFeatures(inst));
		outstep.addRecordLayer(rephid);
		outstep.addRecordLayer(output.getOutput());

		return trial;
	}

	public void record(final TrialStat[] evals)
	{
		System.out.println("============ BEGIN FINAL CLASSIFICATION RECORD ============\n");

		for(int i = 0; i < evals.length; i++)
			System.out.println("Entity " + i + ":\n" + evals[i].toString().replaceAll("\n", "\n    "));

		System.out.println("============== END FINAL CLASSIFICATION RECORD ============\n");
	}

	public void run()
	{
		final TrialSet set = new TrialSet(dataset.getName(), metanet, trials, cfg.getS("foldSplit"),
				cfg.getS("xvalSplit"));

		final Trainer trainer = new Trainer(metanet, set)
		{
			@Override
			public void postEpoch(final int ep, final TestStat stat)
			{
				System.out.println("Completed Epoch " + ep);

				// if we want simultaneous dependency updates
				if(simulDependencyUpdates)
				{
					if(useTrueClassesTrain)
						// if we're using true classes for training, update rep only, not class dep
						for(int i = 0; i < set.nTrainTrials(); i++)
							((NeighborTrial) set.getTrainTrial(i)).updateRepDep();
					else
						// otherwise update both
						for(int i = 0; i < set.nTrainTrials(); i++)
							((NeighborTrial) set.getTrainTrial(i)).updateDependency();

					// update both for test trials
					for(int i = 0; i < set.nTestTrials(); i++)
						((NeighborTrial) set.getTestTrial(i)).updateDependency();

					// update both for validation trials
					for(int i = 0; i < set.nValidationTrials(); i++)
						((NeighborTrial) set.getValidationTrial(i)).updateDependency();
				}
				// if we're not doing simultaneous updates, but we are using true classes in test/not train
				else if(useTrueClassesTest && !useTrueClassesTrain)
				{
					// then we have to reset the training trials to the predicted classes
					for(int i = 0; i < set.nTrainTrials(); i++)
						((NeighborTrial) set.getTrainTrial(i)).updateClassDep();
				}

				if(randomizeNeighbors)
					for(final NeighborTrial trial : trials)
						trial.randomizeNeighborSteps();
			}

			@Override
			public void postFold(final int fold, final TestStat stat)
			{
				System.out.println("Completed Fold " + (fold + 1));
				System.out.println();
			}

			@Override
			public void postTestTrial(final Trial trial, final TrialStat stat)
			{
				// test trial updates the class/rep shown to neighbors (only if updates aren't simul)
				if(!simulDependencyUpdates)
					((NeighborTrial) trial).updateDependency();
			}

			@Override
			public void postTrainTrial(final Trial trial, final TrialStat stat)
			{
				// training trial updates the class it shows to neighbors (only if updates aren't simul)
				if(!simulDependencyUpdates)
					// if we're using true classes for training, update rep only, not class dep
					if(useTrueClassesTrain)
						((NeighborTrial) trial).updateRepDep();
					else
						((NeighborTrial) trial).updateDependency();
			}

			@Override
			public void postValidationTrial(final Trial trial, final TrialStat stat)
			{
				// validation trial updates the class/rep shown to neighbors (only if updates aren't simul)
				if(!simulDependencyUpdates)
					((NeighborTrial) trial).updateDependency();
			}

			@Override
			public void preFold(final int fold)
			{
				System.out.println("Beginning Fold " + (fold + 1));

				// before starting the fold, set training trials to show true classes to neighbors
				if(useTrueClassesTrain)
					for(int i = 0; i < set.nTrainTrials(); i++)
						((NeighborTrial) set.getTrainTrial(i)).setTrueClassDep();
			}

			@Override
			public void preTest(final int fold)
			{
				// before starting the test, set training trials to show true classes to neighbors; but only
				// do it if we didn't already do it in preFold()
				if(useTrueClassesTest && !useTrueClassesTrain)
					for(int i = 0; i < set.nTrainTrials(); i++)
						((NeighborTrial) set.getTrainTrial(i)).setTrueClassDep();
			}
		};

		if(cfg.getB("trainingLogs"))
		{
			try
			{
				trainer.setTrainLog(new TableWriter("train.log", false));
				trainer.setValidationLog(new TableWriter("validation.log", false));
				trainer.setTestLog(new TableWriter("test.log", false));
			}
			catch(final IOException ex)
			{
				ex.printStackTrace();
			}
		}

		System.out.println("Begin Training.");
		final TestStat summary = trainer.run(epochs);

		System.out.println("============= BEGIN FINAL PERFORMANCE SUMMARY =============\n");
		System.out.println(summary);
		System.out.println("=============== END FINAL PERFORMANCE SUMMARY =============\n");

		if(recordFinalClassifications)
			record(trainer.getEvaluations());
	}

	private void setupConfig(final String configfile) throws IOException
	{
		cfg = getDefaultConfig();

		// Try reading actual parameters to replace defaults
		cfg.load(configfile);
	}

	private void setupDataset()
	{
		System.out.println("Loading dataset from files " + cfg.getS("datFile") + ", "
				+ cfg.getS("netFile") + "...");

		final String[] netFiles = cfg.getS("netFile").split(":");
		dataset = new Dataset(cfg.getS("datFile"), netFiles);
	}

	private void setupNetwork()
	{
		useNeighborClasses = cfg.getB("useNeighborClasses");
		useNeighborFeatures = cfg.getB("useNeighborFeatures");
		useNeighborRepresentations = cfg.getB("useNeighborRepresentations");
		useNeighbors = useNeighborFeatures || useNeighborClasses || useNeighborRepresentations;
		useMyHidden = cfg.getI("mySize") > 0;
		trainToFeatures = cfg.getB("trainToFeatures");
		final boolean integrateNeighborInputs = cfg.getB("integrateNeighborInputs");
		final int mySize = cfg.getI("mySize");
		final int theirSize = cfg.getI("theirSize");
		repSize = cfg.getI("repSize");
		final String myType = cfg.getS("myType");
		final String theirType = cfg.getS("theirType");
		final String repType = cfg.getS("repType");
		final String updater = cfg.getS("updaterType");
		final float cp = cfg.getF("connectionProbability");

		final WeightUpdaterType wut = WeightUpdaterType.fromString(updater, cfg.all());
		final WeightInitializer win = new UniformWeightInitializer(cp);

		// -- Create the basic network structure, minus the details of "TheirNet"

		final InputCompound myfeatures = new InputCompound("MyFeatures", dataset.nFeatures());
		MemoryCellCompound myhid = null;
		final MemoryCellCompound rephidmc = new MemoryCellCompound("RepHidden", repSize, repType);
		rephid = rephidmc.getMemoryCells().getOutput();

		output = new XEntropyTargetCompound("OutputXE", dataset.nClasses());

		if(trainToFeatures)
			foutput = new XEntropyTargetCompound("FOutputXE", dataset.nFeatures());

		mynet = new Network("MyNet");
		mynet.setWeightUpdaterType(wut);
		mynet.setWeightInitializer(win);
		mynet.add(myfeatures);
		output.addUpstreamWeights(rephidmc);

		if(foutput != null)
			foutput.addUpstreamWeights(rephidmc);

		if(useMyHidden)
		{
			myhid = new MemoryCellCompound("MyHidden", mySize, myType);
			myhid.addUpstreamWeights(myfeatures);
			rephidmc.addUpstreamWeights(myhid);
			mynet.add(myhid);
		}
		else
		{
			rephidmc.addUpstreamWeights(myfeatures);
		}

		theirnet = new Network("TheirNet");
		theirnet.setWeightUpdaterType(wut);
		theirnet.setWeightInitializer(win);

		outnet = new Network("OutNet");
		outnet.setWeightUpdaterType(wut);
		outnet.setWeightInitializer(win);
		outnet.addTrainOnly(theirnet);
		outnet.add(mynet);
		outnet.add(rephidmc);
		outnet.add(output);
		if(foutput != null)
			outnet.add(foutput);

		// -- Create the "TheirNet" based on config file options

		// create subnetworks for each of the individual input compound types
		if(useNeighbors)
		{
			featurenet = new Network("FeatureNet");
			classnet = new Network("ClassNet");
			repnet = new Network("RepNet");
			ionet = new Network("IONet");

			for(int r = 1; r <= dataset.nRelations(); r++)
			{
				if(useNeighborFeatures)
					addToTheirInputNetwork(featurenet, "Feature", r, dataset.nFeatures());

				if(useNeighborClasses)
					addToTheirInputNetwork(classnet, "Class", r, dataset.nClasses());

				if(useNeighborRepresentations)
					addToTheirInputNetwork(repnet, "Rep", r, repSize);

				addToTheirInputNetwork(ionet, "IO", r, 2);
			}

			// create hidden layer(s) to incorporate the selected inputs
			MemoryCellCompound theirfeature = null, theirclass = null, theirrep = null, theirio = null;

			if(integrateNeighborInputs)
			{
				final MemoryCellCompound theirhid = makeTheirHidden("", theirSize, theirType, rephidmc);
				theirfeature = theirhid;
				theirclass = theirhid;
				theirrep = theirhid;
				theirio = theirhid;
			}
			else
			{
				if(useNeighborFeatures)
					theirfeature = makeTheirHidden("Feature", theirSize, theirType, rephidmc);

				if(useNeighborClasses)
					theirclass = makeTheirHidden("Class", theirSize, theirType, rephidmc);

				if(useNeighborRepresentations)
					theirrep = makeTheirHidden("Rep", theirSize, theirType, rephidmc);

				theirio = makeTheirHidden("IO", theirSize, theirType, rephidmc);
			}

			// connect inputs to hidden layers
			if(useNeighborFeatures)
				connectTheirInputNetwork(featurenet, theirfeature);

			if(useNeighborClasses)
				connectTheirInputNetwork(classnet, theirclass);

			if(useNeighborRepresentations)
				connectTheirInputNetwork(repnet, theirrep);

			connectTheirInputNetwork(ionet, theirio);
		}

		// -- Optimize and build the full network

		metanet = outnet;
		metanet.optimize();
		metanet.build();

		// Find the layer to use as neighbor copy-back representations
		rephid = rephidmc.getOutput();

		if(cfg.getB("networkLog"))
		{
			System.out.println("Actual number of weights: " + outnet.nWeightsDeep());

			final PrintWriter pw = FileTools.createFile("net.log");
			if(useNeighborFeatures)
				pw.println(featurenet.toString("NIC"));
			if(useNeighborClasses)
				pw.println(classnet.toString("NIC"));
			if(useNeighborRepresentations)
				pw.println(repnet.toString("NIC"));
			pw.println(theirnet.toString("NIC"));
			pw.println(mynet.toString("NIC"));
			pw.println(outnet.toString("NIC"));
			pw.close();
		}

		if(cfg.getB("networkDot"))
		{
			final PrintWriter netdot = FileTools.createFile("net.dot");
			netdot.println(new NetworkDotBuilder(metanet).toString());
			netdot.close();
		}
	}

	private void setupTask()
	{
		epochs = cfg.getI("nEpochs");
		recordFinalClassifications = cfg.getB("recordFinalClassifications");
		simulDependencyUpdates = cfg.getB("simulDependencyUpdates");
		useTrueClassesTrain = cfg.getB("useTrueClassesTrain");
		useTrueClassesTest = cfg.getB("useTrueClassesTest");
		randomizeNeighbors = cfg.getB("randomizeNeighbors");
		BitStat.WTA = cfg.getB("wta");
		BitDistStat.WTA = cfg.getB("wta");
	}

	private void setupTrial()
	{
		trials = new NeighborTrial[dataset.nEntities()];

		float[][] classDeps = null, repDeps = null;

		if(useNeighborClasses)
			classDeps = new float[dataset.nEntities()][dataset.nClasses()];

		if(useNeighborRepresentations)
			repDeps = new float[dataset.nEntities()][repSize];

		for(int i = 0; i < trials.length; i++)
		{
			final NeighborTrial trial = makeTrial(i, classDeps, repDeps);
			trials[i] = trial;
		}
	}
}
